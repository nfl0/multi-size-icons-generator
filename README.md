# Multiple Size Icons Generator (.png)

Python program to generate multiple sizes of icons (i.e 16x16, 64x64) from an image

This version generates the following sizes:
 16x16
 32x32
 48x48
 64x64
 128x128
 
#install

Pillow - This library provides extensive file format support, an efficient internal representation, and fairly powerful image processing capabilities.

`pip install Pillow`
